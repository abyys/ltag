//-----------------------------------------------------------------------------
// Pin definitions

#define DOUT  7    // Data input from a microcontroller.
#define DCLK  8    // Data clock from a microcontroller.

// Opcode definitions- the 2-wire serial (which is NOT I2C- it is close to SPI
//  without a CS or MISO, but the Arduino can't clock data slowly enough to
//  suit the widget when using the internal SPI peripheral).
#define OPCODE_PLAY_PAUSE  0xFFFE   // Play a track, or pause the current
                                    //  playback. Note that this requires that
                                    //  you first send a number from 0-511 to
                                    //  the module to indicate which track you
                                    //  wish to play back- simply sending the
                                    //  PLAY_PAUSE opcode has no effect other
                                    //  than pausing playback.
#define OPCODE_STOP        0xFFFF   // Stops the current playback. Subsequent
                                    //  toggling of the PLAY pin will start
                                    //  playback as though no previous track
                                    //  were playing.
#define OPCODE_VOL1         0xFFF1   // Set the volume. 0xFFF0 is muted, 0xFFF7
                                    //  is max. Defaults to max on startup.
#define OPCODE_VOL6         0xFFF6   // Set the volume. 0xFFF0 is muted, 0xFFF7
                                    //  is max. Defaults to max on startup.
                                    
byte file = 1;   // This is a file counter used to internally track the pointer
                 //  for playback. There is no way to read the internal pointer
                 //  and find out which track will play if you simply strobe the
                 //  PLAY pin, or which track you'll be moving to if you strobe
                 //  NEXT or PREV.

void setup()
{
  Serial.begin(9600);
  pinSetup();
}

void loop() 
{
  if (Serial.available() > 0)
  {
    Serial.println("Serial received, play");
    byte command = Serial.read();
    switch(command)
    {
      case 'p':
        sendCommand((unsigned int) file);
        sendCommand(OPCODE_PLAY_PAUSE);
        break;
      case 's':
        sendCommand(OPCODE_STOP);
        break;
      case '+':
        sendCommand(OPCODE_VOL6);
        break;
      case '-':
        sendCommand(OPCODE_VOL1);
        break;
      case 'n':
        if (file++ == 11) file = 0;
        break;
      case 'b':
        if (file-- == 255) file = 10;
        break;
      case 'x':
        sendCommand(OPCODE_PLAY_PAUSE);
        break;
    } 
  }
  if (digitalRead(2)==true){
  sendCommand((unsigned int) file);
  sendCommand(OPCODE_PLAY_PAUSE);   
  Serial.println("taster2 received, play");
  }
  if (digitalRead(4)==true){
  Serial.println("taster4 received, play");
  sendCommand((unsigned int) file);
  sendCommand(OPCODE_PLAY_PAUSE); 
  }
}

void pinSetup()
{
  pinMode(2, INPUT);
  digitalWrite(2, HIGH);
  pinMode(4, INPUT);
  digitalWrite(4, HIGH);
  pinMode(DOUT, OUTPUT);
  digitalWrite(DOUT, HIGH);
  pinMode(DCLK, OUTPUT);
  digitalWrite(DOUT, HIGH);

}


void sendCommand(unsigned int command)
{
  digitalWrite(DCLK, LOW);
  delayMicroseconds(1900);
  for (byte i = 0; i < 16; i++)
  {
    delayMicroseconds(100);
    digitalWrite(DCLK, LOW);
    digitalWrite(DOUT, LOW);
    if ((command & 0x8000) != 0)
    {
      digitalWrite(DOUT, HIGH);
    }
    delayMicroseconds(100);
    digitalWrite(DCLK, HIGH);
    command = command<<1;
  }
}
