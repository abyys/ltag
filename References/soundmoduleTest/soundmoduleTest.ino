#define sck   2
#define sda   4
#define motor 2
#define IR    3
#define IRREC 4
#define R     5
#define G     6
#define B     9
#define l1    14
#define l2    15
#define l3    16
#define Fire  10
#define key   A3



String readString;

void setup() {
   Serial.begin(9600);
  // put your setup code here, to run once:
pinMode(sck, OUTPUT);
digitalWrite(sck,LOW);
pinMode(sda, OUTPUT);
digitalWrite(sda,LOW);
pinMode(Fire, OUTPUT);
digitalWrite(Fire,HIGH);
Serial.print("write to sondmodule: ");
writeSound(6);
}

void loop() {
  // put your main code here, to run repeatedly:
  while (Serial.available()) {
  int n = Serial.parseInt();  // Get char

  writeSound(n);
  delay(1000);
}
}
void writeSound(int n){
  bool datax[9]={0,0,0,0,0,0,0,0,0};
  for (int i = 0; i < 8; ++i) {  // assuming a 32 bit int
  datax[7-i] = n & (1 << i) ? 1 : 0;
  }
  for(int i=0;i<8;i++)
  {Serial.print(datax[i]);}
  Serial.println("");
  digitalWrite(sck,HIGH);
  delay(4);
  digitalWrite(sck,LOW);
  for(int i=0;i<8;i++){
    delayMicroseconds(100);
    digitalWrite(sda,datax[i]);
    delayMicroseconds(400);
    digitalWrite(sck,HIGH);
    delayMicroseconds(500);
    digitalWrite(sck,LOW);
    digitalWrite(sda,LOW);
  }
}
