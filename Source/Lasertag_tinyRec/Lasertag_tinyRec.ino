/* initbit  1bit
 * Spieler  7Bit (max 128
 * team     2bit (max 4
 * damage   4Bit (max 32
 * parity   1bit
 * [0ppppppp]-[ttddddP]
 * maximal IR data 15<=bitcount<=32
 */
#define IRreceivePin  4
#define IRInterupt    2
#define IRClock       1
#define IRData        0
// Signal Properties
int IRpulse = 600; // Basic pulse duration of 600uS MilesTag standard 4*IRpulse for header bit, 2*IRpulse for 1, 1*IRpulse for 0.
int timeOut = IRpulse + 50; // Adding 50 to the expected pulse time gives a little margin for error on the pulse read time out value
bool  debug=false;
bool  data[32];
int   received[32];
int   lengt=0;

void setup() {
  // put your setup code here, to run once:
pinMode(IRInterupt,OUTPUT);
digitalWrite(IRInterupt,LOW);
pinMode(IRreceivePin,INPUT);
pinMode(IRClock,INPUT);
pinMode(IRData,OUTPUT);
digitalWrite(IRData,LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
if(receiveIR()){Sendback();}
}

void Sendback(){
  digitalWrite(IRInterupt,HIGH);
  for(int i =1;i<=lengt;i++){
    while(digitalRead(IRClock)==LOW){}
    digitalWrite(IRData,data[i]);
    while(digitalRead(IRClock)==HIGH){}
  }
  digitalWrite(IRInterupt,LOW);
  digitalWrite(IRData,LOW);
}

bool receiveIR() { // Void checks for an incoming signal and decodes it if it sees one.
  if(digitalRead(IRreceivePin) == LOW){ received[0] = 1; }
  else{ received[0] = 0; }
  while(digitalRead(IRreceivePin) == LOW){ }           //wait for it to turn high again
  for(int i = 1; i <= 32; i++) {                       // Repeats several times to make sure the whole signal has been received
    received[i] = pulseIn(IRreceivePin, LOW, timeOut); // pulseIn command waits for a pulse and then records its duration in microseconds.
  }
  for(int i = 1; i <= 32; i++) {                       // Looks at each one of the received pulses
    int receivedTemp[32];
    receivedTemp[i] = 2;
    if(received[i] > (IRpulse - 200) && received[i] < (IRpulse + 200)) {receivedTemp[i] = 0;} // Works out from the pulse length if it was a data 1 or 0 that was received writes result to receivedTemp string
    if(received[i] > (IRpulse + IRpulse - 200) && received[i] < (IRpulse + IRpulse + 200)) {receivedTemp[i] = 1;} // Works out from the pulse length if it was a data 1 or 0 that was received
    received[i] = 3; // Wipes raw received data
    lengt=i;
    if(receivedTemp[i]<2){ data[i]=(receivedTemp[i]==0 ? false : true);} // Inputs interpreted data
    else{break;}
  }
  
  // Parity Check. Was the data received a valid signal?
  int check = 0;
  bool parity = false;
  for(int i = 1; i <= lengt; i++) {
  if(data[i] == true){check = check + 1;}
  }
  if(check % 2 == 0){parity=true;}
  else{parity=false;}
  if ( lengt >= 13 && parity==data[1] && received[0]==1){ return true;}
  else{return false;}
 
}

