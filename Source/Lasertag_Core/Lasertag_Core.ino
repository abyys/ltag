/*
 * SpielerID  4Bit (max 16)
 * Komando    8Bit (max 32)
 * header,0  p  p  p  p  p  p  p  t  t  d  d  d  d  pa
 *        01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,
 */
#define IRint  A1 
#define IRdat  A0
#define IRclk  13
#define IRled  12
#define RGB    11
#define Sdat   7
#define Sclk   8
#define Motor  9
#define Key1   A6
#define Key2   A7
#define Trigger 4
#define Reload  2
#define l1      3
#define l2      5
#define l3      6
#define shotled 10

  char GameMode     = 's';  //s=standby,d=Deathmatch,t=Teamdeathmatch,c=ctf  
  int PlayerID      = 0;
  int TeamID        = 0;
  int MaxHealth     = 100;
  int Health        = 100;
  int DoubleDamage  = 0;
  int Shield        = 0;

  long Time   = 0;
  long LTime  = 0;
  long count  = 0;
  bool RecBuffer[32];
  bool SendBuffer[32];

int IRpulse                = 600;         // Basic pulse duration of 600uS MilesTag standard 4*IRpulse for header bit, 2*IRpulse for 1, 1*IRpulse for 0.
int IRfrequency            = 56;          // Frequency in kHz Standard values are: 38kHz, 40kHz. Choose dependant on your receiver characteristics
int IRt = ((int) (500/IRfrequency))-4;    // LED on time to give correct transmission frequency, calculated in setup.
int IRpulses = (int) (IRpulse / (2*IRt)); // Number of oscillations needed to make a full IRpulse, calculated in setup.
int header                 = 4;           // Header lenght in pulses. 4 = Miles tag standard
int maxSPS                 = 10;          // Maximum Shots Per Seconds. Not yet used.
int TBS                    = 0;           // Time between shots. Not yet used.


void setup() {
  //Sound Config
  pinMode(Sdat, OUTPUT);
  digitalWrite(Sdat, HIGH);
  pinMode(l1, OUTPUT);
  digitalWrite(l1, HIGH);
  pinMode(l2, OUTPUT);
  digitalWrite(l2, HIGH);
  pinMode(l3, OUTPUT);
  digitalWrite(l3, HIGH);
  pinMode(shotled, OUTPUT);
  digitalWrite(shotled,LOW);
  pinMode(Sdat, OUTPUT);
  digitalWrite(Sdat, HIGH);
  pinMode(Sclk, OUTPUT);
  digitalWrite(Sclk, HIGH);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
//check for inputs 
if(digitalRead(IRint)){
  Serial.println("received IR info");
  ClearRecBuffer();
  ReceiveIR();
  InterpretIR();
  Rumble(50);
  }
if(analogRead(Key1)>1000){}
if(analogRead(Key2)>1000){}
if(digitalRead(Trigger)){shoot(1);}
if(digitalRead(Reload)) {shoot(2);}
if(Serial.available()){Serial.println("serialReceived");}

//Set Animation State

//Process gamedata
}


//----play sound----//
void sendCommand(unsigned int command)
{
 //Start bit Low level pulse.
  digitalWrite(Sclk, LOW);
  // Initialize timer
  delayMicroseconds(0);
  // Wait Start bit length minus 50 us
  delayMicroseconds(1950);
  for (unsigned int mask = 0x8000; mask > 0; mask >>= 1) {
    //Clock low level pulse.
    digitalWrite(Sclk, LOW);
    delayMicroseconds(50);
    //Write data setup.
    if (command & mask) {
      digitalWrite(Sdat, HIGH);
    } else {
      digitalWrite(Sdat, LOW);
    }
    //Write data hold.
    delayMicroseconds(50);
    //Clock high level pulse.
    digitalWrite(Sclk, HIGH);
    delayMicroseconds(100);
  }
  //Stop bit high level pulse (additional 1900us).
  delayMicroseconds(1900);

}
//----Checking received IR data----//

//----shooting----//
void shoot(byte SoundFX) {
    unsigned long StartTime = millis();
    sendCommand((unsigned int) SoundFX); //file
    delay(40);
    Serial.println("Sound played");
    SendBuffer[15]=calcparity();
    sendPulse(header); // Transmit Header pulse, send pulse subroutine deals with the details
    delayMicroseconds(IRpulse);
    for(int i = 1; i < 16; i++) { // Transmit data
      if(SendBuffer[i] == true){sendPulse(1);}
      sendPulse(1);
      delayMicroseconds(IRpulse);
    }
    delay(100);
    digitalWrite(shotled,HIGH);
    analogWrite(Motor,1000);
    delay(100);
    analogWrite(shotled,0);
    digitalWrite(Motor,LOW);
    
unsigned long CurrentTime = millis();
unsigned long ElapsedTime = CurrentTime - StartTime;
Serial.println(ElapsedTime);
  }
bool calcparity(){
  int sum=0;
  for(int i=0;i<15;i++){
    if(SendBuffer[i]==true){sum++;}
  }
  if(!(sum & 1)){return true;}
  else return false;
}
void sendPulse(int length){ // importing variables like this allows for secondary fire modes etc.
// This void genertates the carrier frequency for the information to be transmitted
  int i = 0;
  int o = 0;
  while( i < length ){
    i++;
    while( o < IRpulses ){
      o++;
      digitalWrite(IRled, HIGH);
      delayMicroseconds(IRt);
      digitalWrite(IRled, LOW);
      delayMicroseconds(IRt);
    }
  }
}
void ClearRecBuffer(){
  for(int i=0;i<32;i++){ RecBuffer[i]=0; }
}

void Rumble(int intensity){
  digitalWrite(9,HIGH);
  delay(intensity);
  digitalWrite(9,LOW);
}

//----receiving----//
void ReceiveIR(){
  for(int i=0;i<32;i++){
   digitalWrite(IRclk,HIGH);
   if(digitalRead(IRint)==LOW)break;
   RecBuffer[i]=digitalRead(IRdat);
   delayMicroseconds(300);
   digitalWrite(IRclk,LOW);
   delayMicroseconds(300);
  }
}
bool InterpretIR(){
  if(RecBuffer[0]==0){ Serial.println("Shot packet received"); return true;}
  else {Serial.println("Message Packet received"); return false;}
  
}

