/*Pins
 *          ___________
 *15 GND    | ESP8266 |   8 VCC
 *16 prog   | 12e     |   7 GPIO13  backward
 *17 GPIO02 |         |   6 GPIO12  PWM
 *18 GPIO00 |         |   5 GPIO14  CS
 *19 GPIO04 |         |   4 GPIO16  forward
 *20 GPIO05 |         |   3 CE
 *21 RX     |         |   2 Analog  
 *22 TX     |         |   1 Reset
 *          -----------
 *          | _|-|_-| |
 *          |_________|
 * 
 */

#include <ESP8266WiFi.h>
#include "FS.h"
#include <WebSocketsServer.h>
#include <WiFiClient.h>
#include <TimeLib.h>
#include <NtpClientLib.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <ESP8266mDNS.h>
#include <Ticker.h>
#include <ArduinoOTA.h>
#include <ArduinoJson.h>
#include "FSWebServerLib.h"
#include <Hash.h>
#include <Servo.h> 
Servo Ventil;

#define MotorPWM   16
#define MotorEN    14
#define Ventilpin  12
#define VentilEN   13


WebSocketsServer webSocket = WebSocketsServer(81);

uint16_t      Speed     =0;
uint16_t      MaxSpeed  =0;
uint16_t      MinSpeed  =0;
uint16_t      RandomTime=0;
uint16_t      Freq;
bool          debug     =false;
bool          Broadcast =false;
bool          Arm       =false;
bool          Random    =false;
bool          Up        =false;
bool          Down      =false;
bool          state     =false;
String        data      = "SEND DATA to Clients"; 
String        jsondata  = "";
unsigned long previousMillis = 0;
unsigned long current    =0;
unsigned long previousMillis2 =0;
const long    interval = 350; // update interval


void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) {
    switch(type) {
        case WStype_DISCONNECTED: 
            MaxSpeed=0;
            MinSpeed=0;
            break;
        
        case WStype_CONNECTED: {
            IPAddress ip = webSocket.remoteIP(num);
            webSocket.sendTXT(num, "Connected");
            Broadcast=true;
        }
            break;
            
        case WStype_TEXT: {
            switch(payload[0]) { 
              case 'B' : broadcast_Values();
                         data = "Broadcasting:"; 
                         Broadcast ? data+="On" : data+="Off"; webSocket.broadcastTXT (data) ; 
                         break;
              case 'D' : debug  = !debug; 
                         data = "Debug:"; 
                         debug ? data+="On" : data+="Off"; webSocket.broadcastTXT (data) ; 
                         break;
              case 'f' : Freq = (uint16_t) strtol((const char *) &payload[1], NULL, 10);
                         analogWriteFreq(Freq);
                         if(debug){webSocket.broadcastTXT ("PWMfreq set");} 
                         break;
              case 'X' : MaxSpeed = (uint16_t) strtol((const char *) &payload[1], NULL, 10);
                         if(Arm){ Serial.print("S");
                                  Serial.print(MaxSpeed);
                                  Serial.print("\r\n");
                         }
                         data = "MaxSpeed:"; 
                         if(debug){webSocket.broadcastTXT (data+=MaxSpeed);}  
                         break;
              case 'x' : MinSpeed = (uint16_t) strtol((const char *) &payload[1], NULL, 10);
                         data = "MinSpeed:";
                         if(debug){webSocket.broadcastTXT (data+=MinSpeed);} 
                         break;
              case 'A' : Arm   = true;
                         data = "Armed" ;
                         if(debug){webSocket.broadcastTXT (data) ;}
                         break;
              case 'a' : Arm   = false;
                         data = "Disarmed"; 
                         Serial.print("S0\r\n");
                         if(debug){webSocket.broadcastTXT (data) ;}
                         break;
              case 'R' : Random = true;
                         data = "Random:true" ;
                         if(debug){webSocket.broadcastTXT (data) ;}
                         break;
              case 'r' : Random = false;
                         data = "Random:false"; 
                         if(debug){webSocket.broadcastTXT (data) ;}
                         break;
              case 'U' : Up     = true;
                         data = "Up:true" ;
                         if(debug){webSocket.broadcastTXT (data) ;}
                         break;
              case 'u' : Up     = false;
                         data = "Up:false"; 
                         if(debug){webSocket.broadcastTXT (data) ;}
                         break;
              case 'h' : Down   = true;
                         data = "Down:true" ; 
                         if(debug){webSocket.broadcastTXT (data) ;}
                         break;
              case 'H' : Down   = false;
                         data = "Down:false"; 
                         if(debug){webSocket.broadcastTXT (data) ;} 
                         break;
            }
        break;      
    }}
}

void broadcast_Values(){
  jsondata  = "{" ;
  jsondata += "\"Arm\":" ; 
  if(Arm==true) jsondata += "1"  ;
  else          jsondata += "0"  ;
  jsondata += ", \"Random\":" ;
  if(Random==true) jsondata += "1"  ;
  else          jsondata += "0"  ;
  jsondata += ", \"Speed\":" + String(Speed);
  jsondata += ", \"MaxSpeed\":" + String(MaxSpeed);
  jsondata += ", \"MinSpeed\":" + String(MinSpeed);
  jsondata += ", \"RandomTime\":" + String(RandomTime);
  jsondata += ", \"freq\":" + String(Freq);
  jsondata += "}";
  current=millis();
  if(current>=previousMillis2)
    {webSocket.broadcastTXT(jsondata); 
     previousMillis2=current+10;}
}

void broadcast_Ticker(){
 unsigned long currentMillis = millis();
  if(currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis; 
    if(state==true)  data = "on";
    else             data = "off";
    state = !state;
  webSocket.broadcastTXT(data);
  }
}

void setup() {
    // WiFi is started inside library
    SPIFFS.begin(); // Not really needed, checked inside library and started if needed
    Ventil.attach(Ventilpin);
    ESPHTTPServer.begin(&SPIFFS);
    webSocket.begin();
    webSocket.onEvent(webSocketEvent);
    analogWriteRange(1023);
    analogWriteFreq(18000);
    Serial.begin(9600);
    /* add setup code here */
    pinMode(MotorEN, OUTPUT);
    pinMode(VentilEN, OUTPUT);
    pinMode(MotorPWM, OUTPUT);
    digitalWrite(MotorEN,0);
    digitalWrite(VentilEN,0);
    analogWrite(MotorPWM, 0);

}

void loop() {
    /* add main program code here */
    webSocket.loop();
    ESPHTTPServer.handle();	// DO NOT REMOVE. Attend OTA update from Arduino IDE

}
